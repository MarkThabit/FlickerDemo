//
//  Photo.swift
//  FlickerDemo
//
//  Created by Mark Maged on 3/14/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation

class Photo
{
    let photoID : String
    let farm : Int
    let server : String
    let secret : String
    
    var flickrImageURL: URL? {
        guard let url =  URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret).jpg") else {
            return nil
        }
        
        return url
    }
    
    init?(withData data: [String: Any])
    {
        if let photoID = data["id"] as? String,
            let farm = data["farm"] as? Int,
            let server = data["server"] as? String,
            let secret = data["secret"] as? String
        {
            self.photoID = photoID
            self.farm = farm
            self.server = server
            self.secret = secret
        }
        else
        {
            return nil
        }
    }
    
    class func parsePhotoList(fromData data: [String: Any]) -> [Photo]
    {
        var items = [Photo]()
        
        if let photosContainer = data["photos"] as? [String: AnyObject],
            let photosReceived = photosContainer["photo"] as? [[String: AnyObject]]
        {
            for itemData in photosReceived
            {
                if let photo = Photo(withData: itemData)
                {
                    items.append(photo)
                }
            }
        }
        
        return items
    }
}
