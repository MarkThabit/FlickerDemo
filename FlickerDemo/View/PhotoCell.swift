//
//  PhotoCell.swift
//  FlickerDemo
//
//  Created by Mark Maged on 3/14/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoCell: UICollectionViewCell
{
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - Properties
    
    override var isSelected: Bool {
        didSet
        {
            imageView.layer.borderWidth = isSelected ? 2 : 0
        }
    }
    
    // MARK: - View Life Cycle
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        imageView.layer.borderColor = Constant.kThemeColor.cgColor
        isSelected = false
    }
    
    // MARK: - Actions
    
    func configureCell(photo: Photo)
    {
        self.imageView.sd_setImage(with: photo.flickrImageURL,
                                   placeholderImage: UIImage(named: "placeholder"),
                                   options: .continueInBackground,
                                   completed: nil)
    }
}
