//
//  MainVC.swift
//  FlickerDemo
//
//  Created by Mark Maged on 3/14/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

class MainVC: UIViewController
{
    // MARK: - Private iVars
    
    private let searchController = UISearchController(searchResultsController: nil)
    private let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    private let refreshControl = UIRefreshControl()
    private let itemsPerRow: CGFloat = 3
    private var photos = [Photo]()
    private var pageIndex = 1
    
    // MARK: - Private Computed iVars
    
    private var searchBarIsEmpty: Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private var searchParameters: [String: Any] {
        return ["method"        : "flickr.photos.search",
                "api_key"       : Constant.kAppKey,
                "format"        : "json",
                "nojsoncallback": 1,
                "per_page"      : Constant.kPhotosPerPage,
                "page"          : pageIndex,
                "text"          : self.searchController.searchBar.text!]
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.refreshControl.addTarget(self, action: #selector(self.loadMorePhotos(_:)), for: .valueChanged)
        self.refreshControl.tintColor = .gray
        
        self.collectionView.addSubview(self.refreshControl)
        
        self.configureSearchController()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        self.searchController.isActive = true
    }
    
    // MARK: - Target Actions
    
    @IBAction func loadMorePhotos(_ sender: UIRefreshControl)
    {
        self.pageIndex += 1
        self.initiateRequest { photos in
            self.refreshControl.endRefreshing()
            self.photos.insert(contentsOf: photos, at: 0)
            self.collectionView.reloadData()
        }
    }
    
    // MARK: - Private Methods

    private func configureSearchController()
    {
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Photos"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    @objc private func filterContentForSearchText()
    {
        self.pageIndex = 1
        self.initiateRequest { photos in
            self.photos = photos
            self.collectionView.reloadData()
        }
    }
    
    private func initiateRequest(completion: @escaping ([Photo]) -> Void)
    {
        HTTPRequest.request(parameters: self.searchParameters,
                            successBlock: { photos in
                                completion(photos)
        }) { error in
            print(error)
            completion([])
        }
    }
}

// MARK: - UISearchResultsUpdating Delegate

extension MainVC: UISearchResultsUpdating
{
    func updateSearchResults(for searchController: UISearchController)
    {
        guard !self.searchBarIsEmpty else { return }
        
        NSObject.cancelPreviousPerformRequests(withTarget: self,
                                               selector: #selector(self.filterContentForSearchText),
                                               object: nil)
        
        self.perform(#selector(self.filterContentForSearchText),
                     with: nil,
                     afterDelay: 0.5)
    }
}

// MARK: - UICollectionViewDataSource

extension MainVC: UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoCell
        
        cell.configureCell(photo: self.photos[indexPath.row])
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension MainVC: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return sectionInsets.left
    }
}
