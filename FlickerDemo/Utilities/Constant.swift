//
//  Constant.swift
//  FlickerDemo
//
//  Created by Mark Maged on 3/14/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

struct Constant
{
    static let kAppKey         = "8a0b1d9b34d6146aa73c992a34502e12"
    static let kAppSecretKey   = "a156b06a508664fb"
    static let kThemeColor     = UIColor(red: 0.01, green: 0.41, blue: 0.22, alpha: 1.0)
    static let kFlickerBaseURL = "https://api.flickr.com/services/rest/?"
    static let kPhotosPerPage  = 20
}
