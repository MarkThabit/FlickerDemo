//
//  HTTPReqest.swift
//  FlickerDemo
//
//  Created by Mark Maged on 3/14/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation
import Alamofire

struct HTTPRequest
{
    static func request(url: String = Constant.kFlickerBaseURL,
                        parameters: Parameters,
                        successBlock: @escaping ([Photo]) -> Void,
                        failureBlock: @escaping (Error) -> Void)
    {
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default).validate().responseJSON { response in
                            
                            guard response.error == nil else {
                                failureBlock(response.error!)
                                return
                            }
                            
                            guard let responseDic = response.result.value as? [String: Any] else {
                                let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
                                failureBlock(error)
                                return
                            }
                            
                            let photos = Photo.parsePhotoList(fromData: responseDic)
                            
                            successBlock(photos)
        }
    }
}
